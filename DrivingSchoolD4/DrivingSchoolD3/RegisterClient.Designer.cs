﻿namespace DrivingSchoolD3
{
    partial class RegisterClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.phoneBox = new System.Windows.Forms.TextBox();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.fNBox = new System.Windows.Forms.TextBox();
            this.sNBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.uBox = new System.Windows.Forms.TextBox();
            this.bkBtn = new System.Windows.Forms.Button();
            this.eLevelCBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.enterBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // phoneBox
            // 
            this.phoneBox.Location = new System.Drawing.Point(223, 186);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(153, 20);
            this.phoneBox.TabIndex = 50;
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(223, 150);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(153, 20);
            this.emailBox.TabIndex = 49;
            // 
            // fNBox
            // 
            this.fNBox.Location = new System.Drawing.Point(223, 115);
            this.fNBox.Name = "fNBox";
            this.fNBox.Size = new System.Drawing.Size(153, 20);
            this.fNBox.TabIndex = 48;
            // 
            // sNBox
            // 
            this.sNBox.Location = new System.Drawing.Point(223, 80);
            this.sNBox.Name = "sNBox";
            this.sNBox.Size = new System.Drawing.Size(153, 20);
            this.sNBox.TabIndex = 47;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(223, 44);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(153, 20);
            this.pBox.TabIndex = 46;
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(223, 9);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(153, 20);
            this.uBox.TabIndex = 45;
            // 
            // bkBtn
            // 
            this.bkBtn.Location = new System.Drawing.Point(15, 265);
            this.bkBtn.Name = "bkBtn";
            this.bkBtn.Size = new System.Drawing.Size(113, 21);
            this.bkBtn.TabIndex = 44;
            this.bkBtn.Text = "Back";
            this.bkBtn.UseVisualStyleBackColor = true;
            this.bkBtn.Click += new System.EventHandler(this.bkBtn_Click);
            // 
            // eLevelCBox
            // 
            this.eLevelCBox.FormattingEnabled = true;
            this.eLevelCBox.Items.AddRange(new object[] {
            "Beginner",
            "Experienced"});
            this.eLevelCBox.Location = new System.Drawing.Point(223, 225);
            this.eLevelCBox.Name = "eLevelCBox";
            this.eLevelCBox.Size = new System.Drawing.Size(153, 21);
            this.eLevelCBox.TabIndex = 43;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Experience Level";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Phone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Surname:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Username:";
            // 
            // enterBtn
            // 
            this.enterBtn.Location = new System.Drawing.Point(223, 265);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(153, 21);
            this.enterBtn.TabIndex = 35;
            this.enterBtn.Text = "Enter";
            this.enterBtn.UseVisualStyleBackColor = true;
            this.enterBtn.Click += new System.EventHandler(this.enterBtn_Click);
            // 
            // RegisterClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 302);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.fNBox);
            this.Controls.Add(this.sNBox);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Controls.Add(this.bkBtn);
            this.Controls.Add(this.eLevelCBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enterBtn);
            this.Name = "RegisterClient";
            this.Text = "RegisterClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox phoneBox;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.TextBox fNBox;
        private System.Windows.Forms.TextBox sNBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.Button bkBtn;
        private System.Windows.Forms.ComboBox eLevelCBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button enterBtn;
    }
}