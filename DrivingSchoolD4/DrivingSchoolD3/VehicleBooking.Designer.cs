﻿namespace DrivingSchoolD3
{
    partial class VehicleBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vehicles = new System.Windows.Forms.ComboBox();
            this.vehicleSetBtn = new System.Windows.Forms.Button();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.makeLabel = new System.Windows.Forms.Label();
            this.appointmentBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timebox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // vehicles
            // 
            this.vehicles.FormattingEnabled = true;
            this.vehicles.Location = new System.Drawing.Point(13, 44);
            this.vehicles.Name = "vehicles";
            this.vehicles.Size = new System.Drawing.Size(182, 21);
            this.vehicles.TabIndex = 0;
            this.vehicles.SelectedIndexChanged += new System.EventHandler(this.vehicles_SelectedIndexChanged);
            // 
            // vehicleSetBtn
            // 
            this.vehicleSetBtn.Location = new System.Drawing.Point(446, 44);
            this.vehicleSetBtn.Name = "vehicleSetBtn";
            this.vehicleSetBtn.Size = new System.Drawing.Size(75, 21);
            this.vehicleSetBtn.TabIndex = 2;
            this.vehicleSetBtn.Text = "Set";
            this.vehicleSetBtn.UseVisualStyleBackColor = true;
            this.vehicleSetBtn.Click += new System.EventHandler(this.vehicleSetBtn_Click);
            // 
            // refreshBtn
            // 
            this.refreshBtn.Location = new System.Drawing.Point(365, 44);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(75, 21);
            this.refreshBtn.TabIndex = 3;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // makeLabel
            // 
            this.makeLabel.AutoSize = true;
            this.makeLabel.Location = new System.Drawing.Point(201, 48);
            this.makeLabel.Name = "makeLabel";
            this.makeLabel.Size = new System.Drawing.Size(0, 13);
            this.makeLabel.TabIndex = 4;
            // 
            // appointmentBox
            // 
            this.appointmentBox.FormattingEnabled = true;
            this.appointmentBox.Location = new System.Drawing.Point(13, 90);
            this.appointmentBox.Name = "appointmentBox";
            this.appointmentBox.Size = new System.Drawing.Size(508, 21);
            this.appointmentBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Select Date";
            // 
            // timebox
            // 
            this.timebox.FormattingEnabled = true;
            this.timebox.Location = new System.Drawing.Point(13, 132);
            this.timebox.Name = "timebox";
            this.timebox.Size = new System.Drawing.Size(508, 21);
            this.timebox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Select Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Select License Plate";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 21);
            this.button1.TabIndex = 10;
            this.button1.Text = "Back to Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // VehicleBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 323);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timebox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.appointmentBox);
            this.Controls.Add(this.makeLabel);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.vehicleSetBtn);
            this.Controls.Add(this.vehicles);
            this.Name = "VehicleBooking";
            this.Text = "VehicleBooking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox vehicles;
        private System.Windows.Forms.Button vehicleSetBtn;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.Label makeLabel;
        private System.Windows.Forms.ComboBox appointmentBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox timebox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}