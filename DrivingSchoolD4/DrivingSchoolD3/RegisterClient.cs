﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolD3.controller;
using DrivingSchoolD3.model;

namespace DrivingSchoolD3
{
    public partial class RegisterClient : Form
    {
        public RegisterClient()
        {
            InitializeComponent();
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {

            try
            {
                ClientQueries.InsertNewData(uBox.Text, pBox.Text, sNBox.Text, fNBox.Text, emailBox.Text, phoneBox.Text, eLevelCBox.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            MessageBox.Show("Successfully Registered: " + fNBox.Text + " " + sNBox.Text + ". Your username is: " + uBox.Text);
        }

        private void bkBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}
