﻿namespace DrivingSchoolD3
{
    partial class InstructorAvailAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.refresh = new System.Windows.Forms.Button();
            this.toLogin = new System.Windows.Forms.Button();
            this.setBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.timeCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.instructorCombo = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(141, 145);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(66, 23);
            this.refresh.TabIndex = 15;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // toLogin
            // 
            this.toLogin.Location = new System.Drawing.Point(19, 178);
            this.toLogin.Name = "toLogin";
            this.toLogin.Size = new System.Drawing.Size(111, 23);
            this.toLogin.TabIndex = 14;
            this.toLogin.Text = "Back to Login";
            this.toLogin.UseVisualStyleBackColor = true;
            this.toLogin.Click += new System.EventHandler(this.toLogin_Click);
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(141, 178);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(66, 23);
            this.setBtn.TabIndex = 13;
            this.setBtn.Text = "Set";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Time";
            // 
            // timeCombo
            // 
            this.timeCombo.FormattingEnabled = true;
            this.timeCombo.Items.AddRange(new object[] {
            "9:00am - 10:00am",
            "10:00am - 11.00am",
            "11:00am - 12:00pm",
            "12:00pm - 1:00pm",
            "1:00pm - 2:00pm",
            "2:00pm - 3:00pm",
            "3:00pm - 4:00pm",
            "4:00pm - 5:00pm"});
            this.timeCombo.Location = new System.Drawing.Point(19, 111);
            this.timeCombo.Name = "timeCombo";
            this.timeCombo.Size = new System.Drawing.Size(188, 21);
            this.timeCombo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Pick an Instructor";
            // 
            // instructorCombo
            // 
            this.instructorCombo.FormattingEnabled = true;
            this.instructorCombo.Location = new System.Drawing.Point(19, 25);
            this.instructorCombo.Name = "instructorCombo";
            this.instructorCombo.Size = new System.Drawing.Size(188, 21);
            this.instructorCombo.TabIndex = 9;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(19, 64);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(188, 20);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // InstructorAvailAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 239);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.toLogin);
            this.Controls.Add(this.setBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timeCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.instructorCombo);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "InstructorAvailAdmin";
            this.Text = "InstructorAvailAdmin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Button toLogin;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox timeCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox instructorCombo;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}