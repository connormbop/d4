﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrivingSchoolD3
{
    public partial class ClientHomePage : Form
    {
        public string query = "SELECT IFNAME FROM INSTRUCTOR";
        public string query2 = "";
        public string query3 = "";
        public string date;
        public string instructor;
        public ClientHomePage(string fName, string lName)
        {
            InitializeComponent();
            nameLabel.Text = $"{fName}";
            lNameLabel.Text = $"{lName}";
            SQL.editComboBoxItems(instructComboBox, query);
            
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"INSERT INTO TIMESLOTFINAL(LICENSE, CFNAME, CSNAME, IFNAME, TIMES, DATES) VALUES((SELECT LICENSE FROM VEHICLEBOOKING WHERE DATES = '{dateComboBox.Text}' AND TIMES = '{timeComboBox.Text}'),'{nameLabel.Text}', '{lNameLabel.Text}', '{instructComboBox.Text}', '{timeComboBox.Text}', '{dateComboBox.Text}')");
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            instructor = instructComboBox.Text;
            query3 = $"SELECT DISTINCT DATES FROM INSTRUCTORAVAIL WHERE IFNAME = '{instructor}'";
            SQL.editComboBoxItems(dateComboBox, query3);


            date = dateComboBox.Text;
            if(dateComboBox.Text == "")
            {

            }
            else
            {
                query2 = $"SELECT TIMES FROM TIMEPLUSDATES WHERE DATES = '{date}'";
                SQL.editComboBoxItems(timeComboBox, query2);
            }

        }

        private void BtLBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();

        }
    }
}
