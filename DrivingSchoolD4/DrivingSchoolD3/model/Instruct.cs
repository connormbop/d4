﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivingSchoolD3.model
{
    class Instruct
    {
        public string IUSERNAME { get; set; }
        public string IPASSWORD { get; set; }
        public string ISNAME { get; set; }
        public string IFNAME { get; set; }
        public string IEMAIL { get; set; }
        public string IPHONE { get; set; }
    }
}
