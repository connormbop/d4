﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolD3.controller;

namespace DrivingSchoolD3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cRegoBtn_Click(object sender, EventArgs e)
        {
            new RegisterClient().Show();
            this.Hide();
        }


        private void loginBtn_Click(object sender, EventArgs e)
        {
            string fName;
            string lName;
            if(typeCombo.Text == "Client")
            {
                SQL.selectQuery("SELECT CUSERNAME,CPASSWORD,CSNAME,CFNAME FROM CLIENT");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                        {
                            //cloggedIn = true;
                            fName = SQL.read[3].ToString();
                            lName = SQL.read[2].ToString();
                            new ClientHomePage(fName, lName).Show();
                            this.Hide();
                        }
                    }
                }
            }
            if (typeCombo.Text == "Instructor")
            {
                SQL.selectQuery("SELECT IUSERNAME,IPASSWORD,ISNAME,IFNAME FROM INSTRUCTOR");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                        {
                            //iloggedIn = true;
                            fName = SQL.read[3].ToString();
                            lName = SQL.read[2].ToString();
                            new InstructorAvailability(uBox.Text).Show();
                            this.Hide();
                        }
                    }
                }
            }

            if (typeCombo.Text == "Administrator")
            {
                SQL.selectQuery("SELECT AUSERNAME,APASSWORD,ASNAME,AFNAME FROM ADMINISTRATOR");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                        {
                            //aloggedIn = true;
                            lName = SQL.read[2].ToString();
                            fName = SQL.read[3].ToString();
                            new AdminHP(fName, lName).Show();
                            this.Hide();
                        }
                    }
                }
            }
        }

        private void Staff_Rego_Btn_Click(object sender, EventArgs e)
        {
            new RegisterStaff().Show();
            this.Hide();
        }
    }
}
