﻿namespace DrivingSchoolDFour
{
    partial class adminHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loggedUserLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.carBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.appointIDBox = new System.Windows.Forms.ComboBox();
            this.vSetBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.loadBtn = new System.Windows.Forms.Button();
            this.availStructorsView = new System.Windows.Forms.TreeView();
            this.label6 = new System.Windows.Forms.Label();
            this.instructBox = new System.Windows.Forms.ComboBox();
            this.setBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dayCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.ComboBox();
            this.appointView = new System.Windows.Forms.TreeView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.appIDcombo = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.confirmationBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.appIDEmail = new System.Windows.Forms.ComboBox();
            this.loadAppointments = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.delBtn = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.resignBtn = new System.Windows.Forms.Button();
            this.delCurrentAcct = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.deleteIBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.logBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // loggedUserLabel
            // 
            this.loggedUserLabel.AutoSize = true;
            this.loggedUserLabel.Location = new System.Drawing.Point(10, 10);
            this.loggedUserLabel.Name = "loggedUserLabel";
            this.loggedUserLabel.Size = new System.Drawing.Size(0, 13);
            this.loggedUserLabel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.carBox);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.appointIDBox);
            this.panel1.Controls.Add(this.vSetBtn);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(631, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 340);
            this.panel1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Car";
            // 
            // carBox
            // 
            this.carBox.FormattingEnabled = true;
            this.carBox.Location = new System.Drawing.Point(92, 66);
            this.carBox.Name = "carBox";
            this.carBox.Size = new System.Drawing.Size(151, 21);
            this.carBox.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Appointment ID";
            // 
            // appointIDBox
            // 
            this.appointIDBox.FormattingEnabled = true;
            this.appointIDBox.Location = new System.Drawing.Point(93, 26);
            this.appointIDBox.Name = "appointIDBox";
            this.appointIDBox.Size = new System.Drawing.Size(151, 21);
            this.appointIDBox.TabIndex = 14;
            // 
            // vSetBtn
            // 
            this.vSetBtn.Location = new System.Drawing.Point(92, 107);
            this.vSetBtn.Name = "vSetBtn";
            this.vSetBtn.Size = new System.Drawing.Size(150, 33);
            this.vSetBtn.TabIndex = 17;
            this.vSetBtn.Text = "Set";
            this.vSetBtn.UseVisualStyleBackColor = true;
            this.vSetBtn.Click += new System.EventHandler(this.vSetBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vehicle Availability";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Instructor Availability";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.loadBtn);
            this.panel2.Controls.Add(this.availStructorsView);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.instructBox);
            this.panel2.Controls.Add(this.setBtn);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.dayCombo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.timeBox);
            this.panel2.Location = new System.Drawing.Point(7, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(257, 566);
            this.panel2.TabIndex = 1;
            // 
            // loadBtn
            // 
            this.loadBtn.Location = new System.Drawing.Point(6, 192);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(247, 27);
            this.loadBtn.TabIndex = 13;
            this.loadBtn.Text = "Load Booked Instructors";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // availStructorsView
            // 
            this.availStructorsView.Location = new System.Drawing.Point(4, 225);
            this.availStructorsView.Name = "availStructorsView";
            this.availStructorsView.Size = new System.Drawing.Size(249, 336);
            this.availStructorsView.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Instructor";
            // 
            // instructBox
            // 
            this.instructBox.FormattingEnabled = true;
            this.instructBox.Location = new System.Drawing.Point(86, 31);
            this.instructBox.Name = "instructBox";
            this.instructBox.Size = new System.Drawing.Size(151, 21);
            this.instructBox.TabIndex = 11;
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(86, 131);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(151, 37);
            this.setBtn.TabIndex = 10;
            this.setBtn.Text = "Set";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Time";
            // 
            // dayCombo
            // 
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.Location = new System.Drawing.Point(86, 66);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(151, 21);
            this.dayCombo.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Day";
            // 
            // timeBox
            // 
            this.timeBox.FormattingEnabled = true;
            this.timeBox.Location = new System.Drawing.Point(86, 104);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(151, 21);
            this.timeBox.TabIndex = 7;
            // 
            // appointView
            // 
            this.appointView.Location = new System.Drawing.Point(3, 34);
            this.appointView.Name = "appointView";
            this.appointView.Size = new System.Drawing.Size(348, 272);
            this.appointView.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.appIDcombo);
            this.panel3.Location = new System.Drawing.Point(271, 372);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(354, 219);
            this.panel3.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(76, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 27);
            this.button1.TabIndex = 25;
            this.button1.Text = "Send Invoice Email";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(116, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Appointment ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Billing";
            // 
            // appIDcombo
            // 
            this.appIDcombo.FormattingEnabled = true;
            this.appIDcombo.Location = new System.Drawing.Point(202, 77);
            this.appIDcombo.Name = "appIDcombo";
            this.appIDcombo.Size = new System.Drawing.Size(48, 21);
            this.appIDcombo.TabIndex = 23;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.confirmationBtn);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.appIDEmail);
            this.panel4.Controls.Add(this.loadAppointments);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.appointView);
            this.panel4.Location = new System.Drawing.Point(271, 26);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(354, 340);
            this.panel4.TabIndex = 6;
            // 
            // confirmationBtn
            // 
            this.confirmationBtn.Location = new System.Drawing.Point(145, 310);
            this.confirmationBtn.Name = "confirmationBtn";
            this.confirmationBtn.Size = new System.Drawing.Size(206, 27);
            this.confirmationBtn.TabIndex = 22;
            this.confirmationBtn.Text = "Send Confirmation Email";
            this.confirmationBtn.UseVisualStyleBackColor = true;
            this.confirmationBtn.Click += new System.EventHandler(this.confirmationBtn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 317);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Appointment ID";
            // 
            // appIDEmail
            // 
            this.appIDEmail.FormattingEnabled = true;
            this.appIDEmail.Location = new System.Drawing.Point(91, 312);
            this.appIDEmail.Name = "appIDEmail";
            this.appIDEmail.Size = new System.Drawing.Size(48, 21);
            this.appIDEmail.TabIndex = 6;
            // 
            // loadAppointments
            // 
            this.loadAppointments.Location = new System.Drawing.Point(200, 2);
            this.loadAppointments.Name = "loadAppointments";
            this.loadAppointments.Size = new System.Drawing.Size(152, 31);
            this.loadAppointments.TabIndex = 5;
            this.loadAppointments.Text = "Load Appointments";
            this.loadAppointments.UseVisualStyleBackColor = true;
            this.loadAppointments.Click += new System.EventHandler(this.loadAppointments_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Booked Appointments";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.Controls.Add(this.delBtn);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.deleteIBox);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(631, 372);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(258, 219);
            this.panel5.TabIndex = 7;
            // 
            // delBtn
            // 
            this.delBtn.Location = new System.Drawing.Point(163, 70);
            this.delBtn.Name = "delBtn";
            this.delBtn.Size = new System.Drawing.Size(75, 23);
            this.delBtn.TabIndex = 31;
            this.delBtn.Text = "Delete User";
            this.delBtn.UseVisualStyleBackColor = true;
            this.delBtn.Click += new System.EventHandler(this.delBtn_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Red;
            this.panel6.Controls.Add(this.resignBtn);
            this.panel6.Controls.Add(this.delCurrentAcct);
            this.panel6.Location = new System.Drawing.Point(0, 114);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(140, 104);
            this.panel6.TabIndex = 30;
            // 
            // resignBtn
            // 
            this.resignBtn.Location = new System.Drawing.Point(30, 52);
            this.resignBtn.Name = "resignBtn";
            this.resignBtn.Size = new System.Drawing.Size(81, 37);
            this.resignBtn.TabIndex = 30;
            this.resignBtn.Text = "Resign";
            this.resignBtn.UseVisualStyleBackColor = true;
            this.resignBtn.Click += new System.EventHandler(this.resignBtn_Click);
            // 
            // delCurrentAcct
            // 
            this.delCurrentAcct.AutoSize = true;
            this.delCurrentAcct.Location = new System.Drawing.Point(6, 3);
            this.delCurrentAcct.Name = "delCurrentAcct";
            this.delCurrentAcct.Size = new System.Drawing.Size(126, 17);
            this.delCurrentAcct.TabIndex = 29;
            this.delCurrentAcct.Text = "Delete Current Admin";
            this.delCurrentAcct.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Remove Instructor Account";
            // 
            // deleteIBox
            // 
            this.deleteIBox.FormattingEnabled = true;
            this.deleteIBox.Location = new System.Drawing.Point(146, 37);
            this.deleteIBox.Name = "deleteIBox";
            this.deleteIBox.Size = new System.Drawing.Size(107, 21);
            this.deleteIBox.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Remove Users";
            // 
            // logBtn
            // 
            this.logBtn.Location = new System.Drawing.Point(777, 2);
            this.logBtn.Name = "logBtn";
            this.logBtn.Size = new System.Drawing.Size(115, 21);
            this.logBtn.TabIndex = 14;
            this.logBtn.Text = "Logout";
            this.logBtn.UseVisualStyleBackColor = true;
            this.logBtn.Click += new System.EventHandler(this.logBtn_Click);
            // 
            // adminHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(896, 594);
            this.Controls.Add(this.logBtn);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.loggedUserLabel);
            this.Name = "adminHomePage";
            this.Text = "adminHomePage";
            this.Load += new System.EventHandler(this.adminHomePage_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loggedUserLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView availStructorsView;
        private System.Windows.Forms.TreeView appointView;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox instructBox;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox dayCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox timeBox;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button loadAppointments;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button vSetBtn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox appointIDBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox carBox;
        private System.Windows.Forms.Button confirmationBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox appIDEmail;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox appIDcombo;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox deleteIBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox delCurrentAcct;
        private System.Windows.Forms.Button delBtn;
        private System.Windows.Forms.Button resignBtn;
        private System.Windows.Forms.Button logBtn;
    }
}