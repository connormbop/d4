﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DrivingSchoolDFour.setup;

namespace DrivingSchoolDFour
{
    public partial class InstructorHomePage : Form
    {
        public string query1 = "SELECT TIME FROM TIME";
        public string query2 = "SELECT DAY FROM DAY";
        public string query3 = "SELECT ID FROM APPOINTMENT";
        public string fname;
        public string lname;
        public InstructorHomePage(string fName, string lName)
        {
            InitializeComponent();
            fname = fName;
            lname = lName;
            loggedLabel.Text = $"Logged in as {fname} {lname}";
            SQL.editComboBoxItems(timeBox, query1);
            SQL.editComboBoxItems(dayCombo, query2);
            SQL.editComboBoxItems(appBox, query3);

        }

        private void setBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"INSERT INTO TIMEPLUSDATES(INSTRUCTID,Set_TIMES, Set_DATES) VALUES ((SELECT ID FROM INSTRUCTOR WHERE IFNAME = '{fname}'), '{timeBox.Text}', '{dayCombo.Text}')");
            Connection.con.Close();
            Connection.con.Open();
            SQL.cmd.CommandText = $"select * from TIMEPLUSDATES WHERE INSTRUCTID = (SELECT ID FROM INSTRUCTOR WHERE IFNAME = '{fname}')";
            SQL.cmd.Connection = Connection.con;
            SqlDataReader dr;
            dr = SQL.cmd.ExecuteReader();

            instructBookedTime.Nodes.Clear();

            while (dr.Read())
            {
                TreeNode nodeID = new TreeNode(dr["INSTRUCTID"].ToString());
                TreeNode nodeTime = new TreeNode(dr["Set_TIMES"].ToString());
                TreeNode nodeDate = new TreeNode(dr["Set_DATES"].ToString());

                TreeNode[] array = new TreeNode[] { nodeID, nodeTime, nodeDate };

                TreeNode tnNameofApp = new TreeNode(fname.ToString(), array);
                instructBookedTime.Nodes.Add(tnNameofApp);
            }
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            new Login().Show();
            this.Hide();
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            Connection.con.Close();
            Connection.con.Open();
            SQL.cmd.CommandText = $"select * from APPOINTMENT WHERE ID = '{appBox.Text}'";
            SQL.cmd.Connection = Connection.con;
            SqlDataReader dr;
            dr = SQL.cmd.ExecuteReader();

            while (dr.Read())
            {
                TreeNode nodeID = new TreeNode(dr["ID"].ToString() + " Seen Client!");
                TreeNode nodeLicense = new TreeNode(dr["LICENSE"].ToString());
                TreeNode nodeTime = new TreeNode(dr["TIMES"].ToString());
                TreeNode nodeDate = new TreeNode(dr["DATES"].ToString());
                TreeNode nodeCFName = new TreeNode(dr["CFNAME"].ToString());
                TreeNode nodeCSName = new TreeNode(dr["CEXP"].ToString());
                TreeNode nodeIFName = new TreeNode(dr["IFNAME"].ToString());

                TreeNode[] array = new TreeNode[] { nodeID, nodeLicense, nodeTime, nodeDate, nodeCFName, nodeCSName, nodeIFName };

                TreeNode tnNameofApp = new TreeNode(fname.ToString(), array);
                confTree.Nodes.Add(tnNameofApp);
            }
        }
    }
}
