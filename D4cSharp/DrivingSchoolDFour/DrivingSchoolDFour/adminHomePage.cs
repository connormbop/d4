﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using DrivingSchoolDFour.setup;

namespace DrivingSchoolDFour
{
    public partial class adminHomePage : Form
    {
        public string query1 = "SELECT TIME FROM TIME";
        public string query2 = "SELECT DAY FROM DAY";
        public string query3 = "SELECT ID FROM APPOINTMENT";
        public string query4 = "SELECT MAKE FROM CAR";
        public string query5 = "SELECT ID FROM INSTRUCTOR";
        public string hourV;
        public string dateV;
        public string firstN;
        public string lastN;
        public adminHomePage(string first, string last)
        {
            InitializeComponent();
            firstN = first;
            loggedUserLabel.Text = $"Logged in as {first} {last}";
            SQL.editComboBoxItems(instructBox, query5);
            SQL.editComboBoxItems(timeBox, query1);
            SQL.editComboBoxItems(dayCombo, query2);
            SQL.editComboBoxItems(carBox, query4);
            SQL.editComboBoxItems(appointIDBox, query3);
            SQL.editComboBoxItems(appIDEmail, query3);
            SQL.editComboBoxItems(appIDcombo, query3);
            SQL.editComboBoxItems(deleteIBox, query5);
        }

        private void adminHomePage_Load(object sender, EventArgs e)
        {

        }

        private void loadBtn_Click(object sender, EventArgs e)
        {
            Connection.con.Close();
            Connection.con.Open();
            SQL.cmd.CommandText = $"select * from INSTRUCTOR";
            SQL.cmd.Connection = Connection.con;
            SqlDataReader dr2;
            dr2= SQL.cmd.ExecuteReader();

            availStructorsView.Nodes.Clear();
            while(dr2.Read())
            {
                TreeNode Key1 = new TreeNode($"FIRST NAME: {dr2["IFNAME"].ToString()}");
                TreeNode Key2 = new TreeNode($"ID: {dr2["ID"].ToString()}");


                TreeNode[] arrayLegend = new TreeNode[] { Key1, Key2 };

                TreeNode Key = new TreeNode("Legend:", arrayLegend);
                availStructorsView.Nodes.Add(Key);
            }
            Connection.con.Close();
            Connection.con.Open();
            SQL.cmd.CommandText = $"select * from TIMEPLUSDATES order by Set_DATES + Set_TIMES";
            SQL.cmd.Connection = Connection.con;
            SqlDataReader dr;
            dr = SQL.cmd.ExecuteReader();

            while (dr.Read())
            {
                TreeNode nodeTime = new TreeNode($"{dr["Set_TIMES"].ToString()} ---> {dr["INSTRUCTID"].ToString()}");

                TreeNode[] array = new TreeNode[] {  };

                TreeNode tnNameofApp = new TreeNode($"{dr["Set_DATES"].ToString()} || {dr["Set_TIMES"].ToString()} ---> {dr["INSTRUCTID"].ToString()}", array);
                availStructorsView.Nodes.Add(tnNameofApp);
            }
        }

        private void setBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"INSERT INTO TIMEPLUSDATES(INSTRUCTID,Set_TIMES, Set_DATES) VALUES ((SELECT ID FROM INSTRUCTOR WHERE ID = '{instructBox.Text}'), '{timeBox.Text}', '{dayCombo.Text}')");
        }

        private void loadAppointments_Click(object sender, EventArgs e)
        {
            Connection.con.Close();
            Connection.con.Open();
            SQL.cmd.CommandText = $"select * from APPOINTMENT";
            SQL.cmd.Connection = Connection.con;
            SqlDataReader dr;
            dr = SQL.cmd.ExecuteReader();
            appointView.Nodes.Clear();
            while (dr.Read())
            {
                TreeNode nodeID = new TreeNode(dr["ID"].ToString());
                TreeNode nodeLicense = new TreeNode(dr["LICENSE"].ToString());
                TreeNode nodeTime = new TreeNode(dr["TIMES"].ToString());
                TreeNode nodeDate = new TreeNode(dr["DATES"].ToString());
                TreeNode nodeCFName = new TreeNode(dr["CFNAME"].ToString());
                TreeNode nodeCSName = new TreeNode(dr["CEXP"].ToString());
                TreeNode nodeIFName = new TreeNode(dr["IFNAME"].ToString());

                TreeNode[] array = new TreeNode[] { nodeLicense, nodeTime, nodeDate, nodeCFName, nodeCSName, nodeIFName };

                TreeNode tnNameofApp = new TreeNode($"{dr["ID"].ToString()}", array);
                appointView.Nodes.Add(tnNameofApp);
            }
        }

        private void vSetBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"UPDATE APPOINTMENT SET LICENSE = (SELECT LICENSE FROM CAR WHERE MAKE = '{carBox.Text}') WHERE ID = '{appointIDBox.Text}'");
        }

        private void confirmationBtn_Click(object sender, EventArgs e)
        {
            SQL.cmd.Connection.Close();
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string dbFile = filePath + @"\Billing_Confirmation.txt";

            String sql = $"SELECT * FROM APPOINTMENT WHERE ID = {appIDEmail.Text}";

            SQL.cmd.CommandText = sql;

            SQL.cmd.Connection.Open();
            SqlDataReader sqlReader = SQL.cmd.ExecuteReader();

            // Open the file for write operations.  If exists, it will overwrite due to the "false" parameter
            using (StreamWriter file = new StreamWriter(dbFile, false))
            {
                while (sqlReader.Read())
                {
                    file.WriteLine("Dear " + sqlReader["CFNAME"] + "," + Environment.NewLine + Environment.NewLine + "Your Appointment is the " + sqlReader["DATES"] + " at " + sqlReader["TIMES"] + Environment.NewLine + "We realise that you have are a/an " + sqlReader["CEXP"] + " Driver " + Environment.NewLine + "Your Instructor will be as you selected " + sqlReader["IFNAME"] + Environment.NewLine + Environment.NewLine + "Thanks for Driving with Us!");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SQL.cmd.Connection.Close();
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string dbFile = filePath + @"\Invoice.txt";

            String sql = $"select COST,CLASS from type where CLASS = (SELECT CEXP FROM CLIENT WHERE CFNAME = (SELECT CFNAME FROM APPOINTMENT WHERE ID = '{appIDcombo.Text}'))";

            SQL.cmd.CommandText = sql;

            SQL.cmd.Connection.Open();
            SqlDataReader sqlReader = SQL.cmd.ExecuteReader();

            // Open the file for write operations.  If exists, it will overwrite due to the "false" parameter
            using (StreamWriter file = new StreamWriter(dbFile, false))
            {
                while (sqlReader.Read())
                {
                    file.WriteLine("Dear Client, " + Environment.NewLine + Environment.NewLine + "Please find the attached price amount for your Class with our instructors," + Environment.NewLine + Environment.NewLine + "Thanks again for learning with the DIA." + Environment.NewLine + Environment.NewLine + "Cost:" + sqlReader["COST"] + " For an " + sqlReader["CLASS"] + " Class.");
                }
            }
        
         }

        private void resignBtn_Click(object sender, EventArgs e)
        {
            if(delCurrentAcct.Checked == true)
            {
                SQL.executeQuery($"DELETE FROM ADMINISTRATOR WHERE AFNAME = '{firstN}'");
                new Login().Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Are you sure you wish to resign, If so please tick the checkbox.");
            }
        }

        private void delBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"DELETE FROM TIMEPLUSDATES WHERE INSTRUCTID = {deleteIBox.Text}");
            SQL.executeQuery($"DELETE FROM INSTRUCTOR WHERE ID = '{deleteIBox.Text}'");
            MessageBox.Show("User has been deleted!");
        }

        private void logBtn_Click(object sender, EventArgs e)
        {
            new Login().Show();
            this.Hide();
        }
    }
}
