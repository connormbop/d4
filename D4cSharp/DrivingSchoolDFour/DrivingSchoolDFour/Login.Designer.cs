﻿namespace DrivingSchoolDFour
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clientBtn = new System.Windows.Forms.Button();
            this.instructBtn = new System.Windows.Forms.Button();
            this.adminBtn = new System.Windows.Forms.Button();
            this.clientRegoBtn = new System.Windows.Forms.Button();
            this.staffRegoBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(108, 59);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(100, 20);
            this.uBox.TabIndex = 0;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(321, 59);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(100, 20);
            this.pBox.TabIndex = 1;
            this.pBox.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // clientBtn
            // 
            this.clientBtn.Location = new System.Drawing.Point(121, 101);
            this.clientBtn.Name = "clientBtn";
            this.clientBtn.Size = new System.Drawing.Size(75, 43);
            this.clientBtn.TabIndex = 4;
            this.clientBtn.Text = "Client";
            this.clientBtn.UseVisualStyleBackColor = true;
            this.clientBtn.Click += new System.EventHandler(this.clientBtn_Click);
            // 
            // instructBtn
            // 
            this.instructBtn.Location = new System.Drawing.Point(215, 101);
            this.instructBtn.Name = "instructBtn";
            this.instructBtn.Size = new System.Drawing.Size(75, 43);
            this.instructBtn.TabIndex = 5;
            this.instructBtn.Text = "Instructor";
            this.instructBtn.UseVisualStyleBackColor = true;
            this.instructBtn.Click += new System.EventHandler(this.instructBtn_Click);
            // 
            // adminBtn
            // 
            this.adminBtn.Location = new System.Drawing.Point(309, 101);
            this.adminBtn.Name = "adminBtn";
            this.adminBtn.Size = new System.Drawing.Size(75, 43);
            this.adminBtn.TabIndex = 6;
            this.adminBtn.Text = "Administrator";
            this.adminBtn.UseVisualStyleBackColor = true;
            this.adminBtn.Click += new System.EventHandler(this.adminBtn_Click);
            // 
            // clientRegoBtn
            // 
            this.clientRegoBtn.Location = new System.Drawing.Point(12, 424);
            this.clientRegoBtn.Name = "clientRegoBtn";
            this.clientRegoBtn.Size = new System.Drawing.Size(199, 43);
            this.clientRegoBtn.TabIndex = 8;
            this.clientRegoBtn.Text = "Client Registration";
            this.clientRegoBtn.UseVisualStyleBackColor = true;
            this.clientRegoBtn.Click += new System.EventHandler(this.clientRegoBtn_Click);
            // 
            // staffRegoBtn
            // 
            this.staffRegoBtn.Location = new System.Drawing.Point(273, 424);
            this.staffRegoBtn.Name = "staffRegoBtn";
            this.staffRegoBtn.Size = new System.Drawing.Size(199, 43);
            this.staffRegoBtn.TabIndex = 9;
            this.staffRegoBtn.Text = "Staff Registration";
            this.staffRegoBtn.UseVisualStyleBackColor = true;
            this.staffRegoBtn.Click += new System.EventHandler(this.staffRegoBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(433, 26);
            this.label3.TabIndex = 10;
            this.label3.Text = "Welcome to the Driving Instruction Academies Application for New to Experienced D" +
    "rivers.\r\n                                            Please Sign in or Sign up t" +
    "o Begin!";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 479);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.staffRegoBtn);
            this.Controls.Add(this.clientRegoBtn);
            this.Controls.Add(this.adminBtn);
            this.Controls.Add(this.instructBtn);
            this.Controls.Add(this.clientBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Name = "Login";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clientBtn;
        private System.Windows.Forms.Button instructBtn;
        private System.Windows.Forms.Button adminBtn;
        private System.Windows.Forms.Button clientRegoBtn;
        private System.Windows.Forms.Button staffRegoBtn;
        private System.Windows.Forms.Label label3;
    }
}