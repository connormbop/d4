﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolDFour.controller;

namespace DrivingSchoolDFour
{
    public partial class Login : Form
    {
        public string fName;
        public string lName;
        public Login()
        {
            InitializeComponent();
        }

        private void clientBtn_Click(object sender, EventArgs e)
        {
            SQL.selectQuery("SELECT CUSERNAME,CPASSWORD,CSNAME,CFNAME FROM CLIENT");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                    {
                        //cloggedIn = true;
                        fName = SQL.read[3].ToString();
                        lName = SQL.read[2].ToString();
                        new ClientHomePage(fName, lName).Show();
                        this.Hide();
                    }
                }
            }
        }
    

        private void instructBtn_Click(object sender, EventArgs e)
        {
            SQL.selectQuery("SELECT IUSERNAME,IPASSWORD,ISNAME,IFNAME FROM INSTRUCTOR");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                    {
                        //iloggedIn = true;
                        fName = SQL.read[3].ToString();
                        lName = SQL.read[2].ToString();
                        new InstructorHomePage(fName, lName).Show();
                        this.Hide();
                    }
                }
            }
        }

        private void adminBtn_Click(object sender, EventArgs e)
        {
            SQL.selectQuery("SELECT AUSERNAME,APASSWORD,ASNAME,AFNAME FROM ADMINISTRATOR");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                    {
                        //aloggedIn = true;
                        lName = SQL.read[2].ToString();
                        fName = SQL.read[3].ToString();
                        new adminHomePage(fName, lName).Show();
                        this.Hide();
                    }
                }
            }
        }

        private void clientRegoBtn_Click(object sender, EventArgs e)
        {
            new cSignup().Show();
            this.Hide();
        }

        private void staffRegoBtn_Click(object sender, EventArgs e)
        {
            new sSignup().Show();
            this.Hide();
        }
    }
}
