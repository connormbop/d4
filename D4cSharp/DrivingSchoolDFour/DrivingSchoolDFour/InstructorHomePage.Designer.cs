﻿namespace DrivingSchoolDFour
{
    partial class InstructorHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.setBtn = new System.Windows.Forms.Button();
            this.instructBookedTime = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.ComboBox();
            this.dayCombo = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.confTree = new System.Windows.Forms.TreeView();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.appBox = new System.Windows.Forms.ComboBox();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.loggedLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.setBtn);
            this.panel1.Controls.Add(this.instructBookedTime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.timeBox);
            this.panel1.Controls.Add(this.dayCombo);
            this.panel1.Location = new System.Drawing.Point(12, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 397);
            this.panel1.TabIndex = 0;
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(98, 92);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(121, 37);
            this.setBtn.TabIndex = 5;
            this.setBtn.Text = "Set";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // instructBookedTime
            // 
            this.instructBookedTime.Location = new System.Drawing.Point(18, 230);
            this.instructBookedTime.Name = "instructBookedTime";
            this.instructBookedTime.Size = new System.Drawing.Size(201, 151);
            this.instructBookedTime.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Time";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Day";
            // 
            // timeBox
            // 
            this.timeBox.FormattingEnabled = true;
            this.timeBox.Location = new System.Drawing.Point(98, 65);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(121, 21);
            this.timeBox.TabIndex = 1;
            // 
            // dayCombo
            // 
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.Location = new System.Drawing.Point(98, 27);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(121, 21);
            this.dayCombo.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.confTree);
            this.panel2.Controls.Add(this.confirmBtn);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.appBox);
            this.panel2.Location = new System.Drawing.Point(257, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(314, 278);
            this.panel2.TabIndex = 1;
            // 
            // confTree
            // 
            this.confTree.Location = new System.Drawing.Point(8, 110);
            this.confTree.Name = "confTree";
            this.confTree.Size = new System.Drawing.Size(293, 156);
            this.confTree.TabIndex = 6;
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(182, 54);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(119, 32);
            this.confirmBtn.TabIndex = 7;
            this.confirmBtn.Text = "Confirmed";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Appointment";
            // 
            // appBox
            // 
            this.appBox.FormattingEnabled = true;
            this.appBox.Location = new System.Drawing.Point(78, 27);
            this.appBox.Name = "appBox";
            this.appBox.Size = new System.Drawing.Size(223, 21);
            this.appBox.TabIndex = 0;
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(452, 382);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(119, 41);
            this.logoutBtn.TabIndex = 2;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // loggedLabel
            // 
            this.loggedLabel.AutoSize = true;
            this.loggedLabel.Location = new System.Drawing.Point(14, 9);
            this.loggedLabel.Name = "loggedLabel";
            this.loggedLabel.Size = new System.Drawing.Size(0, 13);
            this.loggedLabel.TabIndex = 3;
            // 
            // InstructorHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 435);
            this.Controls.Add(this.loggedLabel);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "InstructorHomePage";
            this.Text = "InstructorHomePage";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.TreeView instructBookedTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox timeBox;
        private System.Windows.Forms.ComboBox dayCombo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView confTree;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox appBox;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label loggedLabel;
    }
}