﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrivingSchoolDFour.model;

namespace DrivingSchoolDFour.controller
{
    class StaffQueries
    {
        public static List<Instruct> DisplayAllClientsInListInstructor()
        {
            var instructors = new List<Instruct>();
            var dr = SQL.selectQuery("Select * From CLIENT");

            while (dr.Read())
            {
                if (dr.HasRows)
                {
                    instructors.Add(new Instruct { IUSERNAME = $"{dr["IUSERNAME"]}", IPASSWORD = $"{dr["IPASSWORD"]}", ISNAME = $"{dr["ISNAME"]}", IFNAME = $"{dr["IFNAME"]}", IEMAIL = $"{dr["IEMAIL"]}", IPHONE = $"{dr["IPHONE"]}"});
                }
            }

            return instructors;
        }
        public static List<Admin> DisplayAllClientsInListAdmin()
        {
            var admin = new List<Admin>();
            var dr = SQL.selectQuery("Select * From CLIENT");

            while (dr.Read())
            {
                if (dr.HasRows)
                {
                    admin.Add(new Admin { AUSERNAME = $"{dr["AUSERNAME"]}", APASSWORD = $"{dr["APASSWORD"]}", ASNAME = $"{dr["ASNAME"]}", AFNAME = $"{dr["AFNAME"]}", AEMAIL = $"{dr["AEMAIL"]}", APHONE = $"{dr["APHONE"]}" });
                }
            }

            return admin;
        }
        public static int InsertNewDataInstructor(string _iUN, string _iPW, string _iSN, string _iFN, string _iEM, string _iPH)
        {
            var query = $"INSERT INTO INSTRUCTOR VALUES ('{_iUN}','{_iPW}','{_iSN}','{_iFN}','{_iEM}','{_iPH}')";
            return SQL.executeQuery(query);
        }
        public static int InsertNewDataAdmin(string _aUN, string _aPW, string _aSN, string _aFN, string _aEM, string _aPH)
        {
            var query = $"INSERT INTO ADMINISTRATOR VALUES ('{_aUN}','{_aPW}','{_aSN}','{_aFN}','{_aEM}','{_aPH}')";
            return SQL.executeQuery(query);
        }
    }
}
