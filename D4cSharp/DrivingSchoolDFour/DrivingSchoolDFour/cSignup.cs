﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolDFour.controller;

namespace DrivingSchoolDFour
{
    public partial class cSignup : Form
    {
        public cSignup()
        {
            InitializeComponent();
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {

            try
            {
                ClientQueries.InsertNewData(uBox.Text, pBox.Text, sNBox.Text, fNBox.Text, emailBox.Text, phoneBox.Text, eLevelCBox.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            MessageBox.Show("Successfully Registered: " + fNBox.Text + " " + sNBox.Text + ". Your username is: " + uBox.Text);

            new Login().Show();
            this.Hide();
        }

        private void bkBtn_Click(object sender, EventArgs e)
        {
            new Login().Show();
            this.Hide();
        }
    }
}
