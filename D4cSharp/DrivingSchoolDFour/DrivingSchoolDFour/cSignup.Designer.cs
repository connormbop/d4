﻿namespace DrivingSchoolDFour
{
    partial class cSignup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.phoneBox = new System.Windows.Forms.TextBox();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.fNBox = new System.Windows.Forms.TextBox();
            this.sNBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.uBox = new System.Windows.Forms.TextBox();
            this.bkBtn = new System.Windows.Forms.Button();
            this.eLevelCBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.enterBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // phoneBox
            // 
            this.phoneBox.Location = new System.Drawing.Point(103, 184);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(153, 20);
            this.phoneBox.TabIndex = 66;
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(103, 148);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(153, 20);
            this.emailBox.TabIndex = 65;
            // 
            // fNBox
            // 
            this.fNBox.Location = new System.Drawing.Point(103, 116);
            this.fNBox.Name = "fNBox";
            this.fNBox.Size = new System.Drawing.Size(153, 20);
            this.fNBox.TabIndex = 64;
            // 
            // sNBox
            // 
            this.sNBox.Location = new System.Drawing.Point(103, 78);
            this.sNBox.Name = "sNBox";
            this.sNBox.Size = new System.Drawing.Size(153, 20);
            this.sNBox.TabIndex = 63;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(103, 45);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(153, 20);
            this.pBox.TabIndex = 62;
            this.pBox.UseSystemPasswordChar = true;
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(103, 11);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(153, 20);
            this.uBox.TabIndex = 61;
            // 
            // bkBtn
            // 
            this.bkBtn.Location = new System.Drawing.Point(12, 267);
            this.bkBtn.Name = "bkBtn";
            this.bkBtn.Size = new System.Drawing.Size(113, 21);
            this.bkBtn.TabIndex = 60;
            this.bkBtn.Text = "Back";
            this.bkBtn.UseVisualStyleBackColor = true;
            this.bkBtn.Click += new System.EventHandler(this.bkBtn_Click);
            // 
            // eLevelCBox
            // 
            this.eLevelCBox.FormattingEnabled = true;
            this.eLevelCBox.Items.AddRange(new object[] {
            "Beginner",
            "Experienced"});
            this.eLevelCBox.Location = new System.Drawing.Point(103, 223);
            this.eLevelCBox.Name = "eLevelCBox";
            this.eLevelCBox.Size = new System.Drawing.Size(153, 21);
            this.eLevelCBox.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 58;
            this.label7.Text = "Experience:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Phone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "Surname:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Username:";
            // 
            // enterBtn
            // 
            this.enterBtn.Location = new System.Drawing.Point(131, 267);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(125, 21);
            this.enterBtn.TabIndex = 51;
            this.enterBtn.Text = "Enter";
            this.enterBtn.UseVisualStyleBackColor = true;
            this.enterBtn.Click += new System.EventHandler(this.enterBtn_Click);
            // 
            // cSignup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 299);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.fNBox);
            this.Controls.Add(this.sNBox);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Controls.Add(this.bkBtn);
            this.Controls.Add(this.eLevelCBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enterBtn);
            this.Name = "cSignup";
            this.Text = "cSignup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox phoneBox;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.TextBox fNBox;
        private System.Windows.Forms.TextBox sNBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.Button bkBtn;
        private System.Windows.Forms.ComboBox eLevelCBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button enterBtn;
    }
}