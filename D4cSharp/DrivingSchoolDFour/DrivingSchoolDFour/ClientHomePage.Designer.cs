﻿namespace DrivingSchoolDFour
{
    partial class ClientHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.fillHoursBtn = new System.Windows.Forms.Button();
            this.fillDateBtn = new System.Windows.Forms.Button();
            this.setBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tBox = new System.Windows.Forms.ComboBox();
            this.dBox = new System.Windows.Forms.ComboBox();
            this.iBox = new System.Windows.Forms.ComboBox();
            this.loggedLabel = new System.Windows.Forms.Label();
            this.logBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.fillHoursBtn);
            this.panel1.Controls.Add(this.fillDateBtn);
            this.panel1.Controls.Add(this.setBtn);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tBox);
            this.panel1.Controls.Add(this.dBox);
            this.panel1.Controls.Add(this.iBox);
            this.panel1.Location = new System.Drawing.Point(12, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 373);
            this.panel1.TabIndex = 0;
            // 
            // fillHoursBtn
            // 
            this.fillHoursBtn.Location = new System.Drawing.Point(91, 113);
            this.fillHoursBtn.Name = "fillHoursBtn";
            this.fillHoursBtn.Size = new System.Drawing.Size(145, 23);
            this.fillHoursBtn.TabIndex = 11;
            this.fillHoursBtn.Text = "Fill Available Hours";
            this.fillHoursBtn.UseVisualStyleBackColor = true;
            this.fillHoursBtn.Click += new System.EventHandler(this.fillHoursBtn_Click);
            // 
            // fillDateBtn
            // 
            this.fillDateBtn.Location = new System.Drawing.Point(91, 54);
            this.fillDateBtn.Name = "fillDateBtn";
            this.fillDateBtn.Size = new System.Drawing.Size(145, 23);
            this.fillDateBtn.TabIndex = 10;
            this.fillDateBtn.Text = "Fill Available Dates";
            this.fillDateBtn.UseVisualStyleBackColor = true;
            this.fillDateBtn.Click += new System.EventHandler(this.fillDateBtn_Click);
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(91, 185);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(145, 50);
            this.setBtn.TabIndex = 6;
            this.setBtn.Text = "Submit";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Instructor";
            // 
            // tBox
            // 
            this.tBox.FormattingEnabled = true;
            this.tBox.Location = new System.Drawing.Point(91, 142);
            this.tBox.Name = "tBox";
            this.tBox.Size = new System.Drawing.Size(145, 21);
            this.tBox.TabIndex = 2;
            // 
            // dBox
            // 
            this.dBox.FormattingEnabled = true;
            this.dBox.Location = new System.Drawing.Point(91, 85);
            this.dBox.Name = "dBox";
            this.dBox.Size = new System.Drawing.Size(145, 21);
            this.dBox.TabIndex = 1;
            // 
            // iBox
            // 
            this.iBox.FormattingEnabled = true;
            this.iBox.Location = new System.Drawing.Point(91, 27);
            this.iBox.Name = "iBox";
            this.iBox.Size = new System.Drawing.Size(145, 21);
            this.iBox.TabIndex = 0;
            // 
            // loggedLabel
            // 
            this.loggedLabel.AutoSize = true;
            this.loggedLabel.Location = new System.Drawing.Point(9, 9);
            this.loggedLabel.Name = "loggedLabel";
            this.loggedLabel.Size = new System.Drawing.Size(0, 13);
            this.loggedLabel.TabIndex = 1;
            // 
            // logBtn
            // 
            this.logBtn.Location = new System.Drawing.Point(420, 5);
            this.logBtn.Name = "logBtn";
            this.logBtn.Size = new System.Drawing.Size(115, 21);
            this.logBtn.TabIndex = 15;
            this.logBtn.Text = "Logout";
            this.logBtn.UseVisualStyleBackColor = true;
            this.logBtn.Click += new System.EventHandler(this.logBtn_Click);
            // 
            // ClientHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 412);
            this.Controls.Add(this.logBtn);
            this.Controls.Add(this.loggedLabel);
            this.Controls.Add(this.panel1);
            this.Name = "ClientHomePage";
            this.Text = "ClientHomePage";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label loggedLabel;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox tBox;
        private System.Windows.Forms.ComboBox dBox;
        private System.Windows.Forms.ComboBox iBox;
        private System.Windows.Forms.Button fillHoursBtn;
        private System.Windows.Forms.Button fillDateBtn;
        private System.Windows.Forms.Button logBtn;
    }
}