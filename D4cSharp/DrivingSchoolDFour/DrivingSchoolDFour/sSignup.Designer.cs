﻿namespace DrivingSchoolDFour
{
    partial class sSignup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.staffPosBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bkBtn = new System.Windows.Forms.Button();
            this.enterBtn = new System.Windows.Forms.Button();
            this.phoneBox = new System.Windows.Forms.TextBox();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.fNBox = new System.Windows.Forms.TextBox();
            this.sNBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.uBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // staffPosBox
            // 
            this.staffPosBox.FormattingEnabled = true;
            this.staffPosBox.Items.AddRange(new object[] {
            "Instructor",
            "Administrator"});
            this.staffPosBox.Location = new System.Drawing.Point(104, 206);
            this.staffPosBox.Name = "staffPosBox";
            this.staffPosBox.Size = new System.Drawing.Size(100, 21);
            this.staffPosBox.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 58;
            this.label7.Text = "Staff Position:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Phone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "First Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "Surname:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Username:";
            // 
            // bkBtn
            // 
            this.bkBtn.Location = new System.Drawing.Point(8, 246);
            this.bkBtn.Name = "bkBtn";
            this.bkBtn.Size = new System.Drawing.Size(75, 23);
            this.bkBtn.TabIndex = 51;
            this.bkBtn.Text = "Back";
            this.bkBtn.UseVisualStyleBackColor = true;
            this.bkBtn.Click += new System.EventHandler(this.bkBtn_Click);
            // 
            // enterBtn
            // 
            this.enterBtn.Location = new System.Drawing.Point(104, 246);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(100, 23);
            this.enterBtn.TabIndex = 50;
            this.enterBtn.Text = "Enter";
            this.enterBtn.UseVisualStyleBackColor = true;
            this.enterBtn.Click += new System.EventHandler(this.enterBtn_Click);
            // 
            // phoneBox
            // 
            this.phoneBox.Location = new System.Drawing.Point(104, 169);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(100, 20);
            this.phoneBox.TabIndex = 49;
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(104, 136);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(100, 20);
            this.emailBox.TabIndex = 48;
            // 
            // fNBox
            // 
            this.fNBox.Location = new System.Drawing.Point(104, 98);
            this.fNBox.Name = "fNBox";
            this.fNBox.Size = new System.Drawing.Size(100, 20);
            this.fNBox.TabIndex = 47;
            // 
            // sNBox
            // 
            this.sNBox.Location = new System.Drawing.Point(104, 63);
            this.sNBox.Name = "sNBox";
            this.sNBox.Size = new System.Drawing.Size(100, 20);
            this.sNBox.TabIndex = 46;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(104, 33);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(100, 20);
            this.pBox.TabIndex = 45;
            this.pBox.UseSystemPasswordChar = true;
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(104, 5);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(100, 20);
            this.uBox.TabIndex = 44;
            // 
            // sSignup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(217, 280);
            this.Controls.Add(this.staffPosBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bkBtn);
            this.Controls.Add(this.enterBtn);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.fNBox);
            this.Controls.Add(this.sNBox);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Name = "sSignup";
            this.Text = "sSignup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox staffPosBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bkBtn;
        private System.Windows.Forms.Button enterBtn;
        private System.Windows.Forms.TextBox phoneBox;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.TextBox fNBox;
        private System.Windows.Forms.TextBox sNBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox uBox;
    }
}