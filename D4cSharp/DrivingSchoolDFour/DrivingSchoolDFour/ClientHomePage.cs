﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DrivingSchoolDFour.setup;

namespace DrivingSchoolDFour
{
    public partial class ClientHomePage : Form
    {
        public string query1 = "SELECT IFNAME FROM INSTRUCTOR";
        public string hourV;
        public string dateV;
        public string firstN;
        public string lastN;
        public ClientHomePage(string fname, string lname)
        {
            InitializeComponent();
            firstN = fname;
            lastN = lname;
            loggedLabel.Text = $"Logged in as {fname} {lname}";
            SQL.editComboBoxItems(iBox, query1);
        }
 //       ID INT NOT NULL IDENTITY PRIMARY KEY,
	//LICENSE VARCHAR(10),
	//TIMES VARCHAR(50),
	//DATES VARCHAR(50),
	//CFNAME VARCHAR(20),
	//CSNAME VARCHAR(20),
	//IFNAME VARCHAR(20),
        private void setBtn_Click(object sender, EventArgs e)
        {                      
            SQL.executeQuery($"INSERT INTO APPOINTMENT(TIMES,DATES, CFNAME, CEXP, IFNAME) VALUES ('{tBox.Text}' , '{dBox.Text}', '{firstN}', (SELECT CEXP FROM CLIENT WHERE CFNAME = '{firstN}'), '{iBox.Text}')");
        }

        private void fillHoursBtn_Click(object sender, EventArgs e)
        {
            tBox.Text = "";
            hourV = $"SELECT DISTINCT Set_TIMES FROM TIMEPLUSDATES WHERE Set_DATES = '{dBox.Text}'";
            SQL.editComboBoxItems(tBox, hourV);
        }

        private void fillDateBtn_Click(object sender, EventArgs e)
        {
            dBox.Text = "";
            dateV = $"SELECT DISTINCT Set_DATES FROM TIMEPLUSDATES WHERE INSTRUCTID = (SELECT ID FROM INSTRUCTOR WHERE IFNAME = '{iBox.Text}')";
            SQL.editComboBoxItems(dBox, dateV);
        }

        private void logBtn_Click(object sender, EventArgs e)
        {
            new Login().Show();
            this.Hide();
        }
    }
}
